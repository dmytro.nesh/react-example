import { combineReducers } from 'redux';
import { LOGOUT } from 'actionTypes/login';
import login from './login';
import layout from './layout';
import account from './account';
import areas from './areas';
import waiters from './waiters';
import qr from './qr';
import admins from './admins';
import content from './content';
import statistic from './statistic';
import reviews from './reviews';
import menu from './menu';
import alerts from './alerts';

const reducers = combineReducers({
  login,
  layout,
  account,
  areas,
  waiters,
  qr,
  admins,
  content,
  statistic,
  reviews,
  menu,
  alerts
});

const rootReducer = (state, action) => {
  if (action.type === LOGOUT) {
    // eslint-disable-next-line no-param-reassign
    state = undefined;
  }
  return reducers(state, action);
};

export default rootReducer;
