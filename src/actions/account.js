import _get from 'lodash/get';
import { ACCOUNT__SET_ACCOUNT_INFO, ACCOUNT__SET_LOADING } from 'actionTypes/account';
import { get, post, put } from 'utils/api';
import { SUCCESS_MESSAGE, LOGO_UPLOADED_MESSAGE, BACKGROUND_UPLOADED_MESSAGE } from 'actionTypes/alerts';
import {
  setSuccessAlert,
  setErrorAlert
} from 'actions/alerts'

export const setAccountInfo = (data) => (dispatch) => {
  dispatch({
    type: ACCOUNT__SET_ACCOUNT_INFO,
    payload: data,
  });
};

export const setLoading = (status) => (dispatch) => {
  dispatch({
    type: ACCOUNT__SET_LOADING,
    payload: status,
  });
};

export const getAccountInfo = () => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await get('accountInfo');
    dispatch(setAccountInfo(response));
    dispatch(setLoading(false));
  } catch (e) {
    dispatch(setErrorAlert(e.message));
    console.error(e);
  }
};

export const saveAccountInfo = (data) => async (dispatch) => {
  try {
    const newData = {
      ...data
    };
    dispatch(setLoading(true));
    if (data.logo && data.logo.type) {
      const formData = new FormData();
      formData.append('file', data.logo);
      const response = await post(
        'accountInfo/uploadLogo',
        formData,
        {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        }
      );
      dispatch(setSuccessAlert(LOGO_UPLOADED_MESSAGE));
      newData.logo = _get(response, 'logo', '')
    }
    if (data.backgroundImage && data.backgroundImage.type) {
      const formData = new FormData();
      formData.append('file', data.backgroundImage);
      const response = await post(
        'accountInfo/uploadBackground',
        formData,
        {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        }
      );
      dispatch(setSuccessAlert(BACKGROUND_UPLOADED_MESSAGE));
      newData.backgroundImage = _get(response, 'backgroundImage', '')
    }

    const savedAccount = await put(
      'accountInfo',
      {
        ...newData
      },
    );
    if (savedAccount) {
      dispatch(setAccountInfo(savedAccount));
      dispatch(setSuccessAlert(SUCCESS_MESSAGE));
    }
    dispatch(setLoading(false));
  } catch (e) {
    dispatch(setErrorAlert(e.message));
    dispatch(setLoading(false));
    console.error(e);
    throw e;
  }
};
