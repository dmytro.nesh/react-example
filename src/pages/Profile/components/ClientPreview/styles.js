import { makeStyles } from '@material-ui/core/styles';
import tinycolor from 'tinycolor2';

const mainColor = '#37404c';
const secondColor = '#ff812b';

const gradientFinish = (color) => tinycolor(color).brighten(17.5);

const useStyles = makeStyles(() => ({
  clientPreview: {
    width: 200,
    height: 375,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 24,
    borderRadius: 12,
    backgroundColor: '#fff',
    backgroundPosition: 'top',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  },
  homepage: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    padding: 8,
    '& .btn, & .menuBtnLink': {
      display: 'flex',
      alignItems: 'center',
      width: '100%',
      padding: 4,
      borderRadius: 4,
      fontSize: 10,
      fontWeight: 500,
      '& .iconWrapper': {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: 24,
        height: 24,
        borderRadius: 4,
        marginRight: 8,
        fontSize: 14,
      },
      '& .icon': {
        lineHeight: 0,
        display: 'inline-block',
      },
      '& .menuChevron': {
        marginLeft: 'auto',
        marginRight: 4,
      },
    },
    '& .menuBtnLink': {
      background: `linear-gradient(90deg, ${secondColor} -10.93%, ${gradientFinish(secondColor)} 101.1%)`,
      color: '#fff',
      '& .iconWrapper': {
        background: 'rgba(255, 255, 255, 0.17)',
      },
    },
    '& .btn': {
      minHeight: 34,
      boxShadow: '0 4px 16px rgb(218 218 218 / 31%)',
      background: '#fff',
      '& .iconWrapper': {
        background: '#f6f6f7',
      },
    },
    '& header': {
      display: 'flex',
      alignItems: 'center',
      width: '100%',
      '& .icon': {
        padding: '0 6px',
      },
      '& .search': {
        marginLeft: 'auto',
      },
      '& img': {
        width: 52,
      },
    },
    '& .callWaiterSection': {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      marginTop: 12,
      marginBottom: 16,
      '& .callWaiterButton': {
        position: 'relative',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: 44,
        height: 44,
        margin: '8px 0',
        borderRadius: 22,
        fontSize: 20,
        background: `linear-gradient(90deg, ${mainColor} 0%, ${gradientFinish(mainColor)} 100%)`,
        color: '#fff',
        '& .icon': {
          lineHeight: 0,
          display: 'inline-block',
        },
      },
      '& .callWaiterButtonBorder': {
        position: 'absolute',
        fill: '#37404c',
        opacity: .15,
      },
      '& .callWaiterTitle': {
        marginTop: 8,
        fontSize: 10,
        fontWeight: 500,
      },
    },
    '& .content': {
      marginTop: 16,
      marginBottom: 16,
      '& .btn + .btn': {
        marginTop: 6,
      }
    },
    '& .myBookmarks': {
      position: 'absolute',
      right: 16,
      bottom: 16,
      zIndex: 1,
    },
    '& .bookmarksBtn': {
      position: 'relative',
      display: 'flex',
      alignItems: 'center',
      padding: '4px 12px',
      borderRadius: 12,
      fontSize: 8,
      fontWeight: 500,
      border: '1px solid #f6f6f7',
      boxShadow: '0 3px 4px 0 rgb(190 190 190 / 14%), 0 3px 3px -2px rgb(190 190 190 / 12%), 0 1px 8px 0 rgb(190 190 190 / 20%)',
      background: '#fff',
    },
    '& .bookmarksBadge': {
      position: 'absolute',
      width: 12,
      height: 12,
      top: -4,
      right: -2,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      paddingTop: 1,
      borderRadius: 6,
      background: 'linear-gradient(55.78deg, #2da96d 15.03%, #3acf40 119.6%)',
      color: '#fff',
    },
  },
}));

export default useStyles;