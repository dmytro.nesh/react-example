import React, {
  useReducer,
  useEffect,
  useCallback,
  useState,
} from 'react';
import PropTypes from 'prop-types';
import _get from 'lodash/get';
import _isEqual from 'lodash/isEqual';
import { useTranslation } from 'react-i18next';
import {
  Box,
  Button,
  Grid,
  Avatar,
  FormControl,
  FormControlLabel,
  InputLabel,
  InputAdornment,
  TextField,
  Select,
  MenuItem,
  Switch,
  Paper
} from '@material-ui/core';
import {
  Person as DefaultPhoto,
  Close as CloseIcon,
  SaveAlt as SaveIcon,
} from '@material-ui/icons';
import { SketchPicker } from 'react-color';

import { convertToBlobUrl } from 'utils/files';
import { locales } from 'utils/i18n';
import { languages } from 'utils/languages';
import { formReducer } from 'utils/form';
import { ConfirmDialog, NotificationAlert, Typography } from 'components';

import { HoursInputs } from './components/HoursInputs';
import { ClientPreview } from './components/ClientPreview';

import useStyles from './styles';
import { ProfileFormWrapper } from './components/ProfileFormWrapper';

const avatarStyles = { style: { objectFit: 'contain' } };

const initialFormState = {
  name: '',
  email: '',
  color: '',
  textColor: '',
  backgroundColor: '',
  backgroundImage: '',
  isBgColorOn: false,
  isBgImageOn: false,
  logo: '',
  googleMaps: '',
  instagram: '',
  facebook: '',
  wifiNetwork: '',
  wifiPassword: '',
  tgBotLang: 'ru',
  isMenuActive: true,
  isWaiterBellActive: true,
  isReviewsAnchorActive: true,
  isPayBillBtnActive: true,
  hoursList: {
    'Sun': '',
    'Mon': '',
    'Tue': '',
    'Wed': '',
    'Thu': '',
    'Fri': '',
    'Sat': '',
  }
};

const SWITCHERS_NAMES = [
  'isWaiterBellActive',
  'isMenuActive',
  'isPayBillBtnActive',
  'isReviewsAnchorActive',
];

const localesArray = locales.map(langCode => {
  const language = languages[langCode];
  return {
    code: langCode,
    ...language,
  };
});

const Profile = ({
  color,
  textColor,
  backgroundColor,
  backgroundImage,
  isBgColorOn,
  isBgImageOn,
  logo,
  email,
  name,
  instagram,
  facebook,
  googleMaps,
  wifiNetwork,
  wifiPassword,
  tgBotLang,
  isMenuActive,
  isWaiterBellActive,
  isReviewsAnchorActive,
  isPayBillBtnActive,
  accountId,
  hoursList,
  saveAccountInfo,
  getAccountInfo,
  setSuccessAlert,
  setErrorAlert,
  history
}) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const [isOpenPicker, setIsOpenPicker] = useState(false);
  const [isOpenTextPicker, setIsOpenTextPicker] = useState(false);
  const [isOpenBgPicker, setIsOpenBgPicker] = useState(false);
  const [alertOpen, setAlertOpen] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [path, setPath] = useState('');

  // noinspection JSCheckFunctionSignatures
  const [state, dispatch] = useReducer(formReducer, initialFormState);

  const stateFromApi = {
    color,
    textColor,
    backgroundColor,
    backgroundImage,
    isBgColorOn,
    isBgImageOn,
    logo,
    email,
    name,
    instagram,
    facebook,
    googleMaps,
    wifiNetwork,
    wifiPassword,
    tgBotLang,
    isMenuActive,
    isWaiterBellActive,
    isReviewsAnchorActive,
    isPayBillBtnActive,
    hoursList
  }

  useEffect(() => {
    getAccountInfo();
  }, [getAccountInfo]);

  useEffect(() => {
    dispatch({
      newState: {
        color,
        textColor,
        backgroundColor,
        backgroundImage,
        isBgColorOn,
        isBgImageOn,
        logo,
        email,
        name,
        instagram,
        facebook,
        googleMaps,
        wifiNetwork,
        wifiPassword,
        tgBotLang,
        isMenuActive,
        isWaiterBellActive,
        isReviewsAnchorActive,
        isPayBillBtnActive,
        hoursList
      },
    });
  }, [
    email,
    name,
    logo,
    color,
    textColor,
    backgroundColor,
    backgroundImage,
    isBgColorOn,
    isBgImageOn,
    instagram,
    facebook,
    googleMaps,
    wifiNetwork,
    wifiPassword,
    tgBotLang,
    isMenuActive,
    isWaiterBellActive,
    isReviewsAnchorActive,
    isPayBillBtnActive,
    hoursList
  ]);

  const isEqualInfo = _isEqual(state, stateFromApi);

  useEffect(() => {
    const unblock = history.block(({ pathname }) => {
      setPath(pathname);
      if (isEqualInfo) return true;

      setIsModalOpen(true);
      return false;
    });

    return () => unblock();
  });

  const onChange = (e) => {
    const type = _get(e, 'target.type', '');
    const checked = _get(e, 'target.checked', false);
    const field = _get(e, 'target.name');
    const value = type === 'checkbox' ? checked : _get(e, 'target.value', '');
    dispatch({ field, value });
  };
  const onChangeByField = (field, value) => {
    dispatch({ field, value });
  };

  const handleAlertClose = () => {
    setAlertOpen(false);
  };
  const hidePicker = () => setIsOpenPicker(false);
  const hideTextPicker = () => setIsOpenTextPicker(false);
  const hideBgPicker = () => setIsOpenBgPicker(false);
  const showPicker = () => {
    hideTextPicker();
    hideBgPicker();
    setIsOpenPicker(true);
  };
  const showTextPicker = () => {
    hidePicker();
    hideBgPicker();
    setIsOpenTextPicker(true);
  };
  const showBgPicker = () => {
    hidePicker();
    hideTextPicker();
    setIsOpenBgPicker(true);
  };

  const handleChangeColor = newColor => onChangeByField('color', newColor.hex);
  const handleChangeTextColor = newColor => onChangeByField('textColor', newColor.hex);
  const handleChangeBgColor = newColor => onChangeByField('backgroundColor', newColor.hex);

  const handleChangeBgImage = ({ target }) => {
    const type = _get(target, 'type', '');
    const value = type === 'file' ? target.files[0] : target.value;
    onChangeByField('backgroundImage', value);
  };
  const getBgImageName = _get(state, 'backgroundImage.name', state.backgroundImage);

  const handleSwitchBgColor = (e) => {
    onChangeByField('isBgImageOn', false);
    onChange(e);
  };
  const handleSwitchBgImage = (e) => {
    onChangeByField('isBgColorOn', false);
    onChange(e);
  };

  const onResetClientView = () => setAlertOpen(true);
  const setDefaultClientView = () => {
    dispatch({
      newState: {
        color: '',
        textColor: '',
        backgroundColor: '',
        backgroundImage: '',
        isBgColorOn: false,
        isBgImageOn: false,
      },
    });
    setAlertOpen(false);
  };

  const handleChangeAvatar = (event) => {
    onChangeByField('logo', event.target.files[0]);
  };

  const onHoursChange = (data) => {
    dispatch({
      newState: {
        hoursList: data,
      },
    });
  };

  const validateHoursList = useCallback(() => {
    return state.hoursList && Object.keys(state.hoursList).every((dayName) => {
      const value = _get(state.hoursList, `[${dayName}]`, '');
      return /^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]-(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/.test(value) || value === '';
    });
  },
    [state.hoursList],
  );

  const saveChanges = () => {
    const data = {};
    Object.keys(state).forEach((key) => {
      if (typeof state[key] === 'string') {
        data[key] = state[key].trim();
      } else {
        data[key] = state[key];
      }
    });
    const isValidHours = validateHoursList();
    if (!isValidHours) {
      const errorText = t('profile.notValidHours');
      setErrorAlert(errorText);
    } else {
      saveAccountInfo(data).then().catch((e) => {
        if (e && e.response && e.response.status === 413) {
          const errorText = t('common.confirm.fileSizeError');
          setErrorAlert(errorText);
        }
      });
    }
  };

  const onCopyId = useCallback(() => {
    navigator.clipboard.writeText(accountId).then(() => {
      setSuccessAlert('copyIdClipboard');
    });
  }, [accountId, setSuccessAlert]);

  const getApiState = () => dispatch({ newState: stateFromApi })

  const confirmModalHandler = () => {
    saveChanges();
    setIsModalOpen(false);
  }

  const cancelModalHandler = async () => {
    await getApiState();
    setIsModalOpen(false);
    history.push(path);
  }

  return (
    <>
      <ProfileFormWrapper onClickHandler={saveChanges}>
        <Box mb={3}>
          <Typography variant='h4' weight='medium' noWrap>
            {t('profile.sectionTitle.mainInfo')}
          </Typography>
        </Box>
        <Box mb={4}>
          <FormControl>
            <Box display='flex' alignItems='center' flexWrap='wrap'>
              <Avatar
                src={convertToBlobUrl(state.logo) || logo}
                className={classes.avatar}
                imgProps={avatarStyles}
              >
                {!state.logo && !logo && (
                  <DefaultPhoto fontSize='large' />
                )}
              </Avatar>
              <div>
                <input
                  accept='image/*'
                  style={{ display: 'none' }}
                  id='raised-button-file'
                  multiple
                  type='file'
                  onChange={handleChangeAvatar}
                />
                {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
                <label htmlFor='raised-button-file'>
                  <Button
                    variant='contained'
                    component='span'
                    color='primary'
                  >
                    {t('profile.uploadLogo')}
                  </Button>
                </label>
                <Box mt={1}>
                  <Typography color='text' colorBrightness='hint'>
                    {t('profile.maxSizeFile')} - 3 {t('profile.mb')}
                  </Typography>
                </Box>
              </div>
            </Box>
          </FormControl>
        </Box>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <div className={classes.flexIdWrapper}>
              <Box pr={2}>
                <Typography color='text' noWrap>
                  {t('profile.restaurantId')}: <b>{accountId}</b>
                </Typography>
              </Box>
              <Button
                color='primary'
                variant='contained'
                size='small'
                onClick={onCopyId}
              >
                {t('profile.copy')}
              </Button>
            </div>
          </Grid>
          <Grid item xs lg={6}>
            <FormControl fullWidth>
              <TextField
                id='name'
                name='name'
                color='primary'
                label={t('profile.name')}
                value={state.name}
                onChange={onChange}
              />
            </FormControl>
          </Grid>
        </Grid>
      </ProfileFormWrapper>

      <ProfileFormWrapper onClickHandler={saveChanges}>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Typography variant='h4' weight='medium' noWrap>
              {t('profile.sectionTitle.contacts')}
            </Typography>
          </Grid>
          <Grid item xs={12} md={6}>
            <Box mb={2}>
              <FormControl fullWidth>
                <TextField
                  id='email'
                  name='email'
                  color='primary'
                  label='Email'
                  value={state.email}
                  onChange={onChange}
                />
              </FormControl>
            </Box>
            <FormControl fullWidth>
              <TextField
                id='googleMaps'
                name='googleMaps'
                color='primary'
                label={t('profile.googleMaps')}
                value={state.googleMaps}
                onChange={onChange}
              />
            </FormControl>
          </Grid>
          <Grid item xs={12} md={6}>
            <Box mb={2}>
              <FormControl fullWidth>
                <TextField
                  id='instagram'
                  name='instagram'
                  color='primary'
                  label='Instagram'
                  value={state.instagram}
                  onChange={onChange}
                />
              </FormControl>
            </Box>
            <FormControl fullWidth>
              <TextField
                id='facebook'
                name='facebook'
                color='primary'
                label='Facebook'
                value={state.facebook}
                onChange={onChange}
              />
            </FormControl>
          </Grid>
        </Grid>
      </ProfileFormWrapper>

      <ProfileFormWrapper onClickHandler={saveChanges}>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Typography variant='h4' weight='medium' noWrap>
              {t('profile.sectionTitle.wiFiTg')}
            </Typography>
          </Grid>
          <Grid item xs={12} md={6}>
            <Box mb={2}>
              <FormControl fullWidth>
                <TextField
                  id='wifi-ssid'
                  name='wifiNetwork'
                  color='primary'
                  label={t('profile.wifiNetwork')}
                  value={state.wifiNetwork}
                  inputProps={{ maxLength: 31 }}
                  onChange={onChange}
                />
              </FormControl>
            </Box>
            <FormControl fullWidth>
              <TextField
                id='wifi-password'
                name='wifiPassword'
                color='primary'
                label={t('profile.wifiPassword')}
                value={state.wifiPassword}
                inputProps={{ maxLength: 63 }}
                onChange={onChange}
              />
            </FormControl>
          </Grid>
          <Grid item xs={12} md={6}>
            <FormControl fullWidth>
              <InputLabel id='tg-lang-label'>
                {t('profile.langTgBotTitle')}
              </InputLabel>
              <Select
                labelId='tg-lang-label'
                id='tg-lang'
                name='tgBotLang'
                value={state.tgBotLang}
                onChange={onChange}
              >
                {localesArray.map(({ code, native }) => (
                  <MenuItem key={code} value={code}>
                    {code.toUpperCase()}&nbsp;-&nbsp;<small>{native}</small>
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
        </Grid>
      </ProfileFormWrapper>

      <ProfileFormWrapper onClickHandler={saveChanges}>
        <Box mb={3}>
          <Typography variant='h4' weight='medium' noWrap>
            {t('profile.sectionTitle.homepageView')}
          </Typography>
        </Box>
        <div>
          {SWITCHERS_NAMES.map(switchName => (
            <Box key={switchName} mt={1}>
              <FormControlLabel
                name={switchName}
                label={t(`profile.${switchName}`)}
                checked={state[switchName]}
                onChange={onChange}
                control={
                  <Switch
                    color='primary'
                  />
                }
              />
            </Box>
          ))}
        </div>
      </ProfileFormWrapper>

      <ProfileFormWrapper onClickHandler={saveChanges}>
        <Grid container spacing={4}>
          <Grid item xs={12} sm={8} md={6} lg={4}>
            <Typography variant='h4' weight='medium' noWrap>
              {t('profile.sectionTitle.schedule')}
            </Typography>
            <HoursInputs
              defaultHours={hoursList}
              onChange={onHoursChange}
            />
          </Grid>
        </Grid>
      </ProfileFormWrapper>

      {/* Custom client styles */}
      <Box mt={4}>
        <Paper classes={{ root: classes.widgetRoot }}>
          <Grid container spacing={4}>
            <Grid item xs lg={6}>
              <Box pr={{ lg: 8, xl: 15 }}>
                <Typography variant='h4' weight='medium' color='text'>
                  {t('profile.sectionTitle.clientView')}
                </Typography>
                <Box mt={3}>
                  <FormControl fullWidth>
                    <TextField
                      id='color'
                      name='color'
                      color='primary'
                      variant='outlined'
                      label={t('profile.mainColor')}
                      InputLabelProps={{ shrink: true }}
                      InputProps={{
                        startAdornment:
                          <div
                            className={classes.pickerPreview}
                            style={{ backgroundColor: state.color }}
                          />,
                      }}
                      value={state.color}
                      onFocus={showPicker}
                    />
                    {isOpenPicker && (
                      <div className={classes.pickerContainer}>
                        <SketchPicker
                          color={state.color}
                          onChangeComplete={handleChangeColor}
                        />
                        <CloseIcon
                          className={classes.closeBtn}
                          onClick={hidePicker}
                        />
                      </div>
                    )}
                  </FormControl>
                  <Box mt={2.5}>
                    <FormControl fullWidth>
                      <TextField
                        id='textColor'
                        name='textColor'
                        color='primary'
                        variant='outlined'
                        label={t('profile.textColor')}
                        InputLabelProps={{ shrink: true }}
                        InputProps={{
                          startAdornment:
                            <div
                              className={classes.pickerPreview}
                              style={{ backgroundColor: state.textColor }}
                            />,
                        }}
                        value={state.textColor}
                        onFocus={showTextPicker}
                      />
                      {isOpenTextPicker && (
                        <div className={classes.pickerContainer}>
                          <SketchPicker
                            color={state.textColor}
                            onChangeComplete={handleChangeTextColor}
                          />
                          <CloseIcon
                            className={classes.closeBtn}
                            onClick={hideTextPicker}
                          />
                        </div>
                      )}
                    </FormControl>
                  </Box>
                </Box>
                <Box mt={2}>
                  <Typography variant='h6' weight='medium' color='text'>
                    {t('profile.backgroundTitle')}
                  </Typography>

                  <Box mt={2}>
                    <Box mb={2}>
                      <FormControlLabel
                        label={t('profile.backgroundColor')}
                        name='isBgColorOn'
                        checked={state.isBgColorOn}
                        onChange={handleSwitchBgColor}
                        control={
                          <Switch
                            color='primary'
                          />
                        }
                      />
                    </Box>
                    <FormControl fullWidth>
                      <TextField
                        id='backgroundColor'
                        color='primary'
                        variant='outlined'
                        label={t('profile.backgroundColor')}
                        InputLabelProps={{ shrink: true }}
                        InputProps={{
                          startAdornment:
                            <div
                              className={classes.pickerPreview}
                              style={{ backgroundColor: state.backgroundColor }}
                            />,
                        }}
                        value={state.backgroundColor}
                        onFocus={showBgPicker}
                      />
                      {isOpenBgPicker && (
                        <div className={classes.pickerContainer}>
                          <SketchPicker
                            color={state.backgroundColor}
                            onChangeComplete={handleChangeBgColor}
                          />
                          <CloseIcon
                            className={classes.closeBtn}
                            onClick={hideBgPicker}
                          />
                        </div>
                      )}
                    </FormControl>
                  </Box>
                  <Box mt={2}>
                    <Box mb={2}>
                      <FormControlLabel
                        label={t('profile.backgroundImage')}
                        name='isBgImageOn'
                        checked={state.isBgImageOn}
                        onChange={handleSwitchBgImage}
                        control={
                          <Switch
                            color='primary'
                          />
                        }
                      />
                    </Box>
                    <FormControl fullWidth>
                      <TextField
                        id='backgroundImage'
                        color='primary'
                        variant='outlined'
                        label={t('profile.backgroundImage')}
                        value={getBgImageName}
                        InputLabelProps={{ shrink: true }}
                        InputProps={{
                          endAdornment:
                            <InputAdornment position='end'>
                              <label htmlFor='background-image-file'>
                                <Button
                                  variant='contained'
                                  component='span'
                                  color='primary'
                                  startIcon={<SaveIcon />}
                                >
                                  {t('common.btn.upload')}
                                </Button>
                              </label>
                            </InputAdornment>,
                        }}
                        onChange={handleChangeBgImage}
                      />
                      <input
                        accept='image/*'
                        style={{ display: 'none' }}
                        id='background-image-file'
                        type='file'
                        onChange={handleChangeBgImage}
                      />
                    </FormControl>
                  </Box>
                </Box>
                <Box mt={2}>
                  <Button variant='text' color='primary' size='small' onClick={onResetClientView}>
                    {t('profile.resetClientView')}
                  </Button>
                  <Button className={classes.saveBtn} variant='text' color='primary' size='small' onClick={saveChanges}>
                    {t('profile.saveChanges')}
                  </Button>
                </Box>
              </Box>
            </Grid>
            <Grid item xs lg={6}>
              <div className={classes.clientPreviewArea}>
                <Typography variant='h5' weight='medium' color='text'>
                  {t('profile.previewTitle')}
                </Typography>
                <ClientPreview
                  logo={convertToBlobUrl(state.logo) || logo}
                  color={state.color}
                  textColor={state.textColor}
                  backgroundColor={state.backgroundColor}
                  backgroundImage={convertToBlobUrl(state.backgroundImage) || state.backgroundImage}
                  isBgColorOn={state.isBgColorOn}
                  isBgImageOn={state.isBgImageOn}
                />
              </div>
            </Grid>
          </Grid>
        </Paper>
      </Box>


      <ConfirmDialog
        open={alertOpen}
        title={t('profile.resetClientViewQuestion')}
        confirmBtnText={t('common.btn.confirm')}
        cancelBtnText={t('common.btn.cancel')}
        onCancel={handleAlertClose}
        onConfirm={setDefaultClientView}
      />

      <ConfirmDialog
        open={isModalOpen}
        title={t('common.confirm.confirmChanges')}
        cancelBtnText={t('common.btn.confirm')}
        confirmBtnText={t('common.btn.cancel')}
        onConfirm={cancelModalHandler}
        onCancel={confirmModalHandler}
      />

      <NotificationAlert />
    </>
  );
};

Profile.defaultProps = {
  logo: '',
  color: '',
  textColor: '',
  backgroundColor: '',
  backgroundImage: '',
  isBgColorOn: false,
  isBgImageOn: false,
  name: '',
  instagram: '',
  facebook: '',
  googleMaps: '',
  wifiNetwork: '',
  wifiPassword: '',
  tgBotLang: 'ru',
  hoursList: {
    'Sun': '',
    'Mon': '',
    'Tue': '',
    'Wed': '',
    'Thu': '',
    'Fri': '',
    'Sat': '',
  },
  isMenuActive: true,
  isWaiterBellActive: true,
  isReviewsAnchorActive: true,
  isPayBillBtnActive: true,
};

Profile.propTypes = {
  accountId: PropTypes.string.isRequired,
  logo: PropTypes.string,
  color: PropTypes.string,
  textColor: PropTypes.string,
  backgroundColor: PropTypes.string,
  backgroundImage: PropTypes.string,
  isBgColorOn: PropTypes.bool,
  isBgImageOn: PropTypes.bool,
  email: PropTypes.string.isRequired,
  name: PropTypes.string,
  instagram: PropTypes.string,
  facebook: PropTypes.string,
  googleMaps: PropTypes.string,
  wifiNetwork: PropTypes.string,
  wifiPassword: PropTypes.string,
  tgBotLang: PropTypes.string,
  isMenuActive: PropTypes.bool,
  isWaiterBellActive: PropTypes.bool,
  isReviewsAnchorActive: PropTypes.bool,
  isPayBillBtnActive: PropTypes.bool,
  hoursList: PropTypes.shape({
    'Sun': PropTypes.string,
    'Mon': PropTypes.string,
    'Tue': PropTypes.string,
    'Wed': PropTypes.string,
    'Thu': PropTypes.string,
    'Fri': PropTypes.string,
    'Sat': PropTypes.string,
  }),
  saveAccountInfo: PropTypes.func.isRequired,
  getAccountInfo: PropTypes.func.isRequired,
  setSuccessAlert: PropTypes.func.isRequired,
  setErrorAlert: PropTypes.func.isRequired,
  history: PropTypes.shape({
    block: PropTypes.func,
    push: PropTypes.func
  }).isRequired
};

export default React.memo(Profile);
