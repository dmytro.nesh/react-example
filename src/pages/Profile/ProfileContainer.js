import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { saveAccountInfo, getAccountInfo } from 'actions/account';
import {
  setSuccessAlert,
  setErrorAlert
} from 'actions/alerts'
import Profile from './Profile';

const mapStateToProps = ({
  login: { auth },
  account: { accountInfo, loading }
}) => ({
  auth,
  ...accountInfo,
  loading,
});

const mapDispatchToProps = {
  saveAccountInfo,
  getAccountInfo,
  setSuccessAlert,
  setErrorAlert
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps)
)(withRouter(Profile))
