import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  widgetRoot: {
    padding: theme.spacing(4),
    boxShadow: theme.customShadows.widget
  },
  avatar: {
    width: theme.spacing(15),
    height: theme.spacing(15),
    marginRight: theme.spacing(2),
  },
  flexIdWrapper: {
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  pickerPreview: {
    flexShrink: 0,
    width: theme.spacing(4),
    height: theme.spacing(4),
    borderRadius: theme.shape.borderRadius / 2,
    border: '1px solid #dddfe7',
  },
  pickerContainer: {
    position: 'absolute',
    width: 220,
    top: -theme.spacing(1),
    zIndex: 5,
  },
  closeBtn: {
    cursor: 'pointer',
    position: 'absolute',
    top: -theme.spacing(2),
    right: -theme.spacing(4),
  },
  clientPreviewArea: {
    height: '100%',
    padding: theme.spacing(3),
    borderRadius: theme.shape.borderRadius,
    backgroundColor: '#ededed',
  },
  saveBtn: {
    background: 'none',
    border: 'none',
    color: '#4448C9',
    boxShadow: 'none'
  }
}));

export default useStyles;
